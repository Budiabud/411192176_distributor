<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $table = 'supplier';
    protected $primarykey = 'id';

    protected $fillable = [
        'kode_supplier',
        'nama_supplier',
        'alamat', 
        'nama_kota', 
        'no_telepon'
    ];
}
