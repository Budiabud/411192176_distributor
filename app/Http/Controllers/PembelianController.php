<?php

namespace App\Http\Controllers;

use App\Pembelian;
use App\Supplier;
use App\Barang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PembelianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pembelian = Pembelian::latest()->paginate(5);

        return view ('pembelian.index', compact('pembelian'))->with('i', (request()->input('page', 1)-1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $supplier = Supplier::all();
        $barang = Barang::all();
        return view('pembelian.create', compact('barang', 'supplier'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'no_pembelian' => 'required',
            'tanggal' => 'required',
            'id_supplier' => 'required',
            'id_barang' => 'required',
            'jumlah_barang' => 'required',
            'harga_barang' => 'required',
        ]);
        $barang = Barang::find($request->input('id_barang'));
        if(!$barang) {
            return redirect()->back()->withInput()->with('error', 'Data Barang tidak ditemukan');
        }
        if ($request->input('jumlah_barang')<= 0) {
            return redirect()->back()->withInput()->with('error', 'Jumlah Barang harus lebih besar dari 0');
        }
        // if ($barang->stok_barang <= $request->input('jumlah_barang')) {
        //     return redirect()->back()->withInput()->with('error', 'Stok Barang Tidak Cukup');
        // }
        else{
            // Menyimpan nama User setiap dalam penginputan
            $pembelian = new Pembelian($validateData);
            $pembelian->created_by = Auth::user()->name;
            $pembelian->save();
            $barang->stok_barang += $request->input('jumlah_barang');
            $barang->save();
            return redirect()->route('pembelian.index')->with('success','penjualan created Successfully');
        }
   
        return redirect()->route('pembelian.index')->with('success','pembelian created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Pembelian $pembelian)
    {
        return view('pembelian.show', compact('pembelian'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}